//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var proBar: UIProgressView!
    
    @IBOutlet weak var headerLabel: UILabel!
    
    let softTime = 5
    let mediumTime = 7
    let hardTime = 12
    
    var timer: Timer?
    
    var timeLeft = 0
    
    var wholeTime = 0

    @IBAction func actions(_ sender: UIButton) {
        
        headerLabel.text = "How do you like your eggs?"
        
        timeLeft = 0
        
        timer?.invalidate()
        
        proBar.setProgress(0.0, animated: true)
        
        if let textButton = sender.titleLabel?.text {
            
            print(textButton)
//            load(text: textButton)
            
            if textButton == "Soft" {
                wholeTime = softTime
            }
            else if textButton == "Medium" {
                wholeTime = mediumTime
            }
            else if textButton == "Hard" {
                wholeTime = hardTime
            }
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
        }
    }
    
    @objc func onTimerFires() {
        let unit = Float(timeLeft)/Float(wholeTime)
        print(unit)
        proBar.setProgress(unit, animated: true)
        if timeLeft >= wholeTime{
            timer?.invalidate()
            headerLabel.text = "Done!"
        }
        timeLeft += 1
    }
    
}
